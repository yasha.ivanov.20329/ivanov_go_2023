package main

type UserService interface {
	RegisterUser(username, email, password string) error
}

type DefaultUserService struct {
	userRepository UserRepository
}

func NewDefaultUserService(userRepository UserRepository) *DefaultUserService {
	return &DefaultUserService{userRepository: userRepository}
}

func (s *DefaultUserService) RegisterUser(username, email, password string) error {
	user := User{
		ID:       0,
		Username: username,
		Email:    email,
		Password: password,
	}

	err := s.userRepository.Save(user)
	if err != nil {
		return err
	}

	return nil
}
