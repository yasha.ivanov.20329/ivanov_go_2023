package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
)

type UserRepository interface {
	Save(user User) error
}

type FileUserRepository struct {
	filename string
}

func NewFileUserRepository(filename string) *FileUserRepository {
	return &FileUserRepository{filename: filename}
}

func (r *FileUserRepository) Save(user User) error {
	users, err := r.loadUsersFromFile()
	if err != nil {
		return err
	}

	maxID := 0
	for _, u := range users {
		if u.ID > maxID {
			maxID = u.ID
		}
	}

	user.ID = maxID + 1

	users = append(users, user)

	err = r.saveUsersToFile(users)
	if err != nil {
		return err
	}

	return nil
}

func (r *FileUserRepository) loadUsersFromFile() ([]User, error) {
	var users []User

	file, err := os.Open(r.filename)
	if err != nil {
		if os.IsNotExist(err) {
			return users, nil
		}
		return nil, fmt.Errorf("ошибка чтения файла: %v", err)
	}
	defer file.Close()

	data, err := ioutil.ReadAll(file)
	if err != nil {
		return nil, fmt.Errorf("ошибка чтения файла: %v", err)
	}

	err = json.Unmarshal(data, &users)
	if err != nil {
		return nil, fmt.Errorf("ошибка декодирования данных: %v", err)
	}

	return users, nil
}

func (r *FileUserRepository) saveUsersToFile(users []User) error {
	data, err := json.MarshalIndent(users, "", "  ")
	if err != nil {
		return fmt.Errorf("ошибка кодирования данных: %v", err)
	}

	err = ioutil.WriteFile(r.filename, data, 0644)
	if err != nil {
		return fmt.Errorf("ошибка записи файла: %v", err)
	}

	return nil
}
