package main

func main() {
	fileUserRepository := NewFileUserRepository("users.json")
	userService := NewDefaultUserService(fileUserRepository)
	userController := NewUserController(userService)

	userController.RegisterUser()
}
