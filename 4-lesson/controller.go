package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

type UserController struct {
	userService UserService
}

func NewUserController(userService UserService) *UserController {
	return &UserController{userService: userService}
}

func (c *UserController) RegisterUser() {
	reader := bufio.NewReader(os.Stdin)

	fmt.Print("Введите имя пользователя: ")
	username, _ := reader.ReadString('\n')

	fmt.Print("Введите email: ")
	email, _ := reader.ReadString('\n')

	fmt.Print("Введите пароль: ")
	password, _ := reader.ReadString('\n')

	err := c.userService.RegisterUser(username, email, password)
	if err != nil {
		log.Fatalf("Ошибка регистрации пользователя: %v", err)
	}

	fmt.Println("Регистрация пользователя завершена. Данные сохранены.")
}
