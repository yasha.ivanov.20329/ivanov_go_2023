package main

import "fmt"

type ArrayProcessor interface {
	ProcessArrayToArray([]int) []int
	ProcessArrayToNumber([]int) int
}

type SumAndZeroProcessor struct {
}

func (s SumAndZeroProcessor) ProcessArrayToArray(arr []int) []int {
	processedArray := make([]int, 0)

	for _, num := range arr {
		if num != 0 {
			processedArray = append(processedArray, num)
		}
	}

	return processedArray
}

func (s SumAndZeroProcessor) ProcessArrayToNumber(arr []int) int {
	sum := 0

	for _, num := range arr {
		sum += num
	}

	return sum
}

type MultiAndNegativeProcessor struct{}

func (m MultiAndNegativeProcessor) ProcessArrayToArray(arr []int) []int {
	processedArray := make([]int, 0)

	for _, num := range arr {
		if num >= 0 {
			processedArray = append(processedArray, num)
		}
	}

	return processedArray
}

func (m MultiAndNegativeProcessor) ProcessArrayToNumber(arr []int) int {
	product := 1

	for _, num := range arr {
		product *= num
	}

	return product
}

func ProcessArray(arr []int, processor ArrayProcessor) ([]int, int) {
	processedArray := processor.ProcessArrayToArray(arr)
	number := processor.ProcessArrayToNumber(arr)
	return processedArray, number
}

func main() {
	arr := []int{1, 2, 3, 0, -4, 5, -6}

	sumAndZeroProcessor := SumAndZeroProcessor{}
	multiAndNegativeProcessor := MultiAndNegativeProcessor{}

	resultArray, resultNumber := ProcessArray(arr, sumAndZeroProcessor)
	fmt.Println("--sumAndZeroProcessor--")
	fmt.Println("Result array: ", resultArray)
	fmt.Println("Result number: ", resultNumber)

	resultArray, resultNumber = ProcessArray(arr, multiAndNegativeProcessor)
	fmt.Println("--multiAndNegativeProcessor--")
	fmt.Println("Result array: ", resultArray)
	fmt.Println("Result number: ", resultNumber)
}
