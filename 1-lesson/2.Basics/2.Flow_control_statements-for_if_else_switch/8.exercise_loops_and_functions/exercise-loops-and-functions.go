package main

import (
	"fmt"
	"math"
)

func Sqrt(x float64) float64 {
	z := 1.0
	previousZ := 0.0
	for i := 0; i < 10; i++ {
		z -= (z*z - x) / (2 * z)
		if math.Abs(previousZ-z) < 1e-10 {
			break
		}
		previousZ = z
		fmt.Println(z)
	}
	return z
}

func main() {
	fmt.Println(Sqrt(3))
}
