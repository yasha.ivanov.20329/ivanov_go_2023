package main

import (
	"fmt"
	"time"
)

type Document struct {
	description string
	name        string
	copies      int
	startDate   time.Time
	endDate     time.Time
}

func (doc Document) PrintInfo() {
	fmt.Printf("Название: %s\n", doc.name)
	fmt.Printf("Описание: %s\n", doc.description)
	fmt.Printf("Копий: %d\n", doc.copies)
	fmt.Printf("Начало: %s\n", doc.startDate.Format("2006-01-02"))
	fmt.Printf("Конец: %s\n", doc.endDate.Format("2006-01-02"))
}

func (doc *Document) SetCopies(copies int) {
	if copies >= 0 {
		doc.copies = copies
	}
}

func (doc *Document) SetDescription(description string) {
	doc.description = description
}

func main() {
	doc := Document{
		name:        "Тестовый документ",
		description: "lorem ipsum",
		copies:      66,
		startDate:   time.Date(2022, time.April, 13, 12, 30, 30, 500, time.UTC),
		endDate:     time.Date(2023, time.February, 23, 0, 0, 0, 0, time.UTC),
	}

	doc.SetDescription("Новое описание документа")
	doc.SetCopies(5)
	doc.PrintInfo()
}
